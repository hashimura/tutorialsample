var Tutorial = (function () {
    var instance = null;

    function PrivateConstructor() {

        var arrow_pos = "top";
        var top;
        var left;
        var right;
        var nr_of_steps = 0;
        var pos = 0;
        var tutorials = new Array();
        var container = $('#guide_container');
        var prev_btn = $('#guide_prev');
        var next_btn = $('#guide_next');
        var arrow = $('#guide_arrow');
        var progress = $('#guide_progressed');
        var progress_total = $('#guide_of_total');
        var display_tutorial = false;

        var curr_highlighted_element;
        var curr_tutorial_element;
        var curr_route;
        var tutorial_container = $('#tutorial_container');

        // Public functions
        this.Initialize = function (display) {

                display_tutorial = display
                SetupCallbacks();
                SetupGuideBtn();
        }

        this.SetCurrentRoute = function(r)
        {
            curr_route = r;
        }

        var SetupGuideBtn = function()
        {

            if(HasTutorial){
                var _container = $('article.content');
                var _el = $('<div/>',{'id':'guide_button','class':'btn btn_small btn_link'});
                _el.append('Guide');
                _el.bind("click",DisplayTutorial);

                _container.prepend(_el);
                $('section.relative:eq(0)').removeClass('first');
            }

        }

        var DisplayTutorial = function()
        {
            
            ShowTutorials();
            $('#tutorial_container').show();
        }

        var SetupCallbacks = function () {

            $('#guide_prev,#guide_next').live('click', function (event) {
                CallBackPaginate(event, $(this));
            });
            $('#guide_close_button').live('click', function () {
                CallBackCloseGuide($(this));
            });
            $(window).resize(function () {
                CallBackWindowResize()
            });

            if(display_tutorial == 1){
                ShowTutorials();
            }
        }


        var CallBackPaginate = function (event, e) {

            if (event.target.id == "guide_next") {
                var btn = next_btn;
                var nextpos = pos + 1;
            } else {
                var btn = prev_btn;
                var nextpos = pos - 1;
            }

            if (btn.hasClass('btn_disabled')) return false;

            var tutorial_active = tutorials[pos];
            var tutorial_next = tutorials[nextpos];

            event.target.id == "guide_next" ? pos++ : pos--;

            //Positioning lets place the box correctly
            PositionElementHighLight(tutorial_next, tutorial_active);

            CheckPagination(nextpos);
        }

        var PositionElementHighLight = function (tutorial_element_next, tutorial_element_active) {
            var highlight_curr = $("#" + tutorial_element_active.attr('element'));
            var highlight_next = $("#" + tutorial_element_next.attr('element'));
            var position = CalculateTutorialPopupPlacement(highlight_next, container, $('#guide_arrow'));

            container.css(position);
           
            $('html, body').animate({ scrollTop: container.offset().top-80 }, 500);
            //Remove highlight and add on next
            highlight_curr.removeClass('guide_highlight');
            highlight_next.addClass('guide_highlight');

            tutorial_element_active.css('display', 'none');
            tutorial_element_next.css('display', 'block');
        }

        var CallBackWindowResize = function () {
            var position = CalculateTutorialPopupPlacement(curr_highlighted_element, container, $('#guide_arrow'));
            container.css(position);
        }

        var CallBackCloseGuide = function (e) {
            //save db for user not to show guide anymore...
            $.post('/tutorial/ajax/closeguide',{'route':curr_route},function(resp){
               var r = $.parseJSON(resp);
                if(r.error!=undefined){
                    alert(r.message);
                }
            });
            $('#tutorial_container').hide();
        }

        var CheckPagination = function (pos) {


            if (pos == (nr_of_steps - 1)) {
                next_btn.addClass('btn_disabled');
            } else {
                next_btn.removeClass('btn_disabled');
            }

            if (pos == 0) {
                prev_btn.addClass('btn_disabled');
            } else if (pos > 0) {
                prev_btn.removeClass('btn_disabled');
            }
            progress.text(pos + 1);
        }

        var ShowTutorials = function () {
            $(".content_row").each(function () {
                tutorials.push($(this));
                nr_of_steps++
            });

            if (tutorials.length == 0) return false;
            var first = tutorials[0];
            var tutorial_block = first.attr('element');

            $("#" + tutorial_block).addClass('guide_highlight');
            first.css('display', 'block');

            var position = CalculateTutorialPopupPlacement($('#' + tutorial_block), container, $('#guide_arrow'));
            container.css(position);
            progress_total.text(nr_of_steps);

        }

        var HasTutorial = function()
        {
           return  tutorials.length > 0 ? true : false;
        }

        var CalculateTutorialPopupPlacement = function (i_highlighted_element, i_tutorial_element, i_arrow_element)
        {
            curr_highlighted_element = i_highlighted_element;
            curr_tutorial_element = i_tutorial_element;

            var _position = i_highlighted_element.offset();
            var _width = i_highlighted_element.outerWidth();
            var _height = i_highlighted_element.outerHeight();

            var _centered_position = {left:_position.left + _width * 0.5, top:_position.top + _height * 0.5};
            var _scroll_offset = {left:$(document).scrollLeft(), top:$(document).scrollTop()};
            var _ss_centered_position = {left:_centered_position.left - _scroll_offset.left, top:_centered_position.top - _scroll_offset.top};
            var _ss_center_offset = {left:_ss_centered_position.left - $(window).width() * 0.5, top:_ss_centered_position.top - $(window).height() * 0.5}
            
            // Arrow height
            var _vertical_offset = i_arrow_element.outerHeight();
            
            if (_ss_center_offset.left < 0)
            {
                if (_ss_center_offset.top < 0)
                {
                    // Upper Left
                    i_arrow_element.removeClass();
                    i_arrow_element.addClass('top_left');
                    return {left:_position.left, top:_position.top + _height + _vertical_offset};
                }
                else
                {
                    // Lower Left
                    i_arrow_element.removeClass();
                    i_arrow_element.addClass('bottom_left');
                    return {left:_position.left, top:_position.top - i_tutorial_element.outerHeight() - _vertical_offset};
                }
            }
            else
            {
                if (_ss_center_offset.top < 0)
                {
                    // Upper Right
                    i_arrow_element.removeClass();
                    i_arrow_element.addClass('top_right');
                    return {left:_position.left + _width - i_tutorial_element.outerWidth(), top:_position.top + _height + _vertical_offset};
                }
                else
                {
                    // Lower Right
                    i_arrow_element.removeClass();
                    i_arrow_element.addClass('bottom_right');
                    return {left:_position.left + _width - i_tutorial_element.outerWidth(), top:_position.top - i_tutorial_element.outerHeight() - _vertical_offset};
                }
            }
        }
    }

    return new function () {
        this.getInstance = function () {
            if (instance == null) {
                instance = new PrivateConstructor();
                instance.constructor = null;
            }
            return instance;
        }
    }
})();