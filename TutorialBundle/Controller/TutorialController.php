<?php

    namespace SprinkleBit\TutorialBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use SprinkleBit\CoreBundle\Entity\UsersTutorial;

    class TutorialController extends Controller
    {

        public function RenderTutorialAction($route)
        {
            // matched routes has available tutorials

            $template = true;

            switch ($route) {
                case '_profile':
                case '_activity':
                    $route = "_profile";
                break;

                case '_portfolio':break;
                case '_orders':break;
                case '_friends':break;
                case '_about':break;
                case '_marketplace':break;
                case '_visualizer':break;
                case '_recent_orders':break;
                case '_stock_listing':break;
                case '_education_chapter':break;

                default:
                    $template = false;
            }
            $user_id   = $this->get('security.context')->getToken()->getUser()->getId();

            if ($template)
            {
                $display = $this->checkDisplay($user_id,$route);
                return $this->render('SprinkleBitTutorialBundle::tutorials.html.twig', array('route' => $route, 'display'=>$display));
            } else {
                return new Response('');
            }

        }

        public function CloseGuideUserAction(Request $request)
        {

            $route   = $request->request->get('route');
            $user_id = $this->get('security.context')->getToken()->getUser()->getId();
            $cache   = $this->get('sonata.cache.memcached');
            $key     = "tutorial_".$user_id;

            if ($request->isXmlHttpRequest() === false) {
                return new Response("This method only accept ajax requests");
            } elseif ($request->getMethod() != "POST") {
                return new Response('This method only accept POST');
            }elseif(empty($route)){
                return new Response('Route parameter needs to be provided');
            }

            try
            {
                $check   = $this->getDoctrine()->getRepository('SprinkleBitCoreBundle:UsersTutorial')->findOneBy(array('user_id'=>$user_id,'route'=>$route));
                if (!is_object($check)) {
                    $em = $this->getDoctrine()->getEntityManager();
                    $t  = new UsersTutorial();
                    $t->setUserId($user_id);
                    $t->setRoute($route);
                    $em->persist($t);
                    $em->flush();

                    //empty cache
                    if ($cache->get(array($key))) {
                        $cache->flush(array($key));
                    }
                }

            } catch (\Exception $e) {
                return new Response(json_encode(array('error'=>1,'message'=>'There was an error in the request')));
            }

            return new Response(json_encode(array('success'=>1)));
        }

        private function checkDisplay($user_id, $route)
        {
            $cache   = $this->get('sonata.cache.memcached');
            $key     = "tutorial_".$user_id;

            // If no cache, generate a new one.
            if(!$cache->has(array($key)))
            {
                $display = array();
                $tutorials_users    = $this->getDoctrine()->getRepository('SprinkleBitCoreBundle:UsersTutorial')->findBy(array('user_id' => $user_id));

                foreach($tutorials_users as $t){
                    $display[] = $t->getRoute();
                }
                $cache->set(array($key),serialize($display),86400);
            }
            else
            {
                $display = unserialize($cache->get(array($key))->getData());
            }

            if(in_array($route,$display)){
                return 0;
            }

            return 1;
        }
    }
